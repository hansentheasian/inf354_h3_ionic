import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { APIService } from '../shared/api.service';
import { Customer } from '../shared/customer.model';
import { Orderitems } from '../shared/orderitems.model';

@Component({
  selector: 'app-create-order-item',
  templateUrl: './create-order-item.component.html',
  styleUrls: ['./create-order-item.component.scss'],
})
export class CreateOrderItemComponent implements OnInit {

  constructor(
    private formbuilder: FormBuilder,
    public router : Router,
    public orderitemservice:APIService
  ) { }

  OrderItemForm:any;
  data = false;
  message : string;
  ngOnInit(){
  
    this.OrderItemForm = this.formbuilder.group({
      HotDogID:['',[Validators.required]],
      Quantity:['',[Validators.required]]
    })
    this.orderitemservice.formData4={
      OrderItemID:0,
      HotDogID: 0,
      Quantity: 0
    }
  }
  SaveOrderItem(){
    
    this.router.navigate(['/orderitem'])
    console.log(this.orderitemservice.formData4);
    this.orderitemservice.AddOrderItem(this.orderitemservice.formData4).subscribe(res => {
      console.log(res);

      
    })
  }
  onFormSubmit(OrderItemForm){
    const orderitem = OrderItemForm.value;
    this.CreateOrderItem(orderitem)
  }

  CreateOrderItem(orderitem: Orderitems){
    this.orderitemservice.AddOrderItem(orderitem).subscribe(()=>{
      this.data = true;
      this.message = 'Saved';
      this.OrderItemForm.reset();
    })
  }
  GoBackToList(){
    this.router.navigate(['/orderitem'])
  }

}
