import { Component, OnInit } from '@angular/core';
import { Customer } from '../shared/customer.model';
import { APIService } from '../shared/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
})
export class CustomerComponent implements OnInit {
 
  formData:Customer;
  CustomerList:Customer[];
  

  constructor(
  public CustomerService:APIService,
  private router:Router
  ) { }

  ngOnInit() {
    this.resetForm();
    this.refreshList();
  }
  
  refreshList(){
    this.CustomerService.getCustomerList().then(res => this.CustomerList = res as Customer[]);
  }
 
  resetForm() {
    this.formData = {
      CustomerID: 0,
      Name: '',
      Surname: ''
    }
  }
  GoHome(){
    this.router.navigate(['/homepage'])
  }
  AddCustomer(){
    this.router.navigate(['/create-customer'])
  }
  DeleteCustomer(id:number){
    this.CustomerService.DeleteCustomer(id).subscribe(res =>{
      this.resetForm();
      this.refreshList();
    })
  }
  EditCustomer(hdObj : Customer){
    // this.router.navigate(['/update-order-item', {id : HotDogID}])
    this.resetForm();
    this.refreshList();
    this.formData.CustomerID = hdObj.CustomerID;
    this.formData.Name = hdObj.Name;
    this.formData.Surname = hdObj.Surname;
  }

  SaveCustomer() {
    this.CustomerService.UpdateCustomer(this.formData).subscribe(res => {
      console.log(res);
      this.resetForm();
      this.refreshList();
    })
  }
  
  
}

