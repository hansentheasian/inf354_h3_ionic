import { Component, OnInit } from '@angular/core';
import { APIService} from '../shared/api.service';
import { Router } from '@angular/router';
import { HotDog } from '../shared/hot-dog.model';



@Component({
  selector: 'app-hot-dogs',
  templateUrl: './hot-dogs.component.html',
  styleUrls: ['./hot-dogs.component.scss'],
})
export class HotDogsComponent implements OnInit {
  
  formData:HotDog;
  HotDogList:HotDog[];
  

  constructor(
  public HotDogService:APIService,
  private router:Router
  ) { }

  ngOnInit() {
    this.resetForm();
    this.refreshList();
  }
  
  refreshList(){
    this.HotDogService.getHotDogList().then(res => this.HotDogList = res as HotDog[]);
  }
 
  resetForm() {
    this.formData = {
      HotDogID: 0,
      Name: '',
      Price: 0
    }
  }
  GoHome(){
    this.router.navigate(['/homepage'])
  }
  AddHotDog(){
    this.router.navigate(['/create-hot-dog'])
  }
  DeleteHotDog(id:number){
    this.HotDogService.DeleteHotDog(id).subscribe(res =>{
      this.resetForm();
      this.refreshList();
    })
  }
  EditHotDog(hdObj : HotDog){
    // this.router.navigate(['/update-order-item', {id : HotDogID}])
    this.resetForm();
    this.refreshList();
    this.formData.HotDogID = hdObj.HotDogID;
    this.formData.Name = hdObj.Name;
    this.formData.Price = hdObj.Price;
  }

  SaveHotDog() {
    this.HotDogService.UpdateHotDog(this.formData).subscribe(res => {
      console.log(res);
      this.resetForm();
      this.refreshList();
    })
  }
  
  
}
