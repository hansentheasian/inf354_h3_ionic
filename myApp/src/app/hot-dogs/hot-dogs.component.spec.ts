import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HotDogsComponent } from './hot-dogs.component';

describe('HotDogsComponent', () => {
  let component: HotDogsComponent;
  let fixture: ComponentFixture<HotDogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotDogsComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HotDogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
