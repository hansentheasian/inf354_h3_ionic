import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {

  constructor(
    public router:Router
  ) { }

  ngOnInit():void{}

  OrderItems(){
    this.router.navigate(['/orderitem'])
  }

  HotDogs(){
    this.router.navigate(['/hot-dogs'])
  }
  Users(){
    this.router.navigate(['/users'])
  }
  Customers(){
    this.router.navigate(['/customer'])
  }
  Orders(){
    this.router.navigate(['/orders'])
  }
}
