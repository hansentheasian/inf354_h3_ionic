import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { APIService } from '../shared/api.service';
import { HotDog } from '../shared/hot-dog.model';
import { Customer } from '../shared/customer.model';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss'],
})
export class CreateCustomerComponent implements OnInit {

  constructor(
    private formbuilder: FormBuilder,
    public router : Router,
    public customerservice:APIService
  ) { }

  CustomerForm:any;
  data = false;
  message : string;
  ngOnInit(){
  
    this.CustomerForm = this.formbuilder.group({
      Name:['',[Validators.required]],
      Surname:['',[Validators.required]]
    })
    this.customerservice.formData3={
      CustomerID:0,
      Name : '',
      Surname: ''
    }
  }
  SaveCustomer(){
    
    this.router.navigate(['/customer'])
    console.log(this.customerservice.formData3);
    this.customerservice.AddCustomer(this.customerservice.formData3).subscribe(res => {
      console.log(res);

      
    })
  }
  onFormSubmit(CustomerForm){
    const customer = CustomerForm.value;
    this.CreateCustomer(customer)
  }

  CreateCustomer(customer: Customer){
    this.customerservice.AddCustomer(customer).subscribe(()=>{
      this.data = true;
      this.message = 'Saved';
      this.CustomerForm.reset();
    })
  }
  GoBackToList(){
    this.router.navigate(['/customer'])
  }

}
