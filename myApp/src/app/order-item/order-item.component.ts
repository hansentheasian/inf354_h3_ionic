import { Component, OnInit } from '@angular/core';
import { Orderitems } from '../shared/orderitems.model';
import { APIService } from '../shared/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.scss'],
})
export class OrderItemComponent implements OnInit {

  formData:Orderitems;
  OrderItemList:Orderitems[];
  

  constructor(
  public OrderItemService:APIService,
  private router:Router
  ) { }

  ngOnInit() {
    this.resetForm();
    this.refreshList();
  }
  
  refreshList(){
    this.OrderItemService.getOrderItemList().then(res => this.OrderItemList = res as Orderitems[]);
  }
 
  resetForm() {
    this.formData = {
      OrderItemID: 0,
      HotDogID: 0,
      Quantity: 0
    }
  }
  GoHome(){
    this.router.navigate(['/homepage'])
  }
  AddOrderItem(){
    this.router.navigate(['/create-orderitem'])
  }
  DeleteOrderItem(id:number){
    this.OrderItemService.DeleteOrderItem(id).subscribe(res =>{
      this.resetForm();
      this.refreshList();
    })
  }
  EditOrderItem(hdObj : Orderitems){
    // this.router.navigate(['/update-order-item', {id : HotDogID}])
    this.resetForm();
    this.refreshList();
    this.formData.OrderItemID = hdObj.OrderItemID;
    this.formData.HotDogID = hdObj.HotDogID;
    this.formData.Quantity = hdObj.Quantity;
  }

  SaveOrderItem() {
    this.OrderItemService.UpdateOrderItem(this.formData).subscribe(res => {
      console.log(res);
      this.resetForm();
      this.refreshList();
    })
  }
  
  
}