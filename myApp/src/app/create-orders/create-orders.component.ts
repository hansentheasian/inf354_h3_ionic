import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { APIService } from '../shared/api.service';
import { Orderitems } from '../shared/orderitems.model';
import { Orders } from '../shared/orders.model';

@Component({
  selector: 'app-create-orders',
  templateUrl: './create-orders.component.html',
  styleUrls: ['./create-orders.component.scss'],
})
export class CreateOrdersComponent implements OnInit {

  constructor(
    private formbuilder: FormBuilder,
    public router : Router,
    public orderservice:APIService
  ) { }

  OrderForm:any;
  data = false;
  message : string;
  ngOnInit(){
  
    this.OrderForm = this.formbuilder.group({
      OrderNo:['',[Validators.required]],
      CustomerID:['',[Validators.required]],
      PayMethod:['',[Validators.required]]
    })
    this.orderservice.formData5={
      OrderID:0,
      OrderNo:'',
      CustomerID:0,
      PayMethod:''
    }
  }
  SaveOrder(){
    
    this.router.navigate(['/orders'])
    console.log(this.orderservice.formData5);
    this.orderservice.AddOrder(this.orderservice.formData5).subscribe(res => {
      console.log(res);

      
    })
  }
  onFormSubmit(OrderForm){
    const order = OrderForm.value;
    this.CreateOrder(order)
  }

  CreateOrder(order: Orders){
    this.orderservice.AddOrder(order).subscribe(()=>{
      this.data = true;
      this.message = 'Saved';
      this.OrderForm.reset();
    })
  }
  GoBackToList(){
    this.router.navigate(['/orders'])
  }

}
