import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, Router } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HotDogsComponent } from './hot-dogs/hot-dogs.component';
import { HttpClientModule } from '@angular/common/http';
import { CreateHotDogComponent } from './create-hot-dog/create-hot-dog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserComponent } from './user/user.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { CustomerComponent } from './customer/customer.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { OrderItemComponent } from './order-item/order-item.component';
import { CreateOrderItemComponent } from './create-order-item/create-order-item.component';
import { OrdersComponent } from './orders/orders.component';
import { CreateOrdersComponent } from './create-orders/create-orders.component';
import { HomePageComponent } from './home-page/home-page.component';


@NgModule({
  declarations: [AppComponent,
  HotDogsComponent,
  CreateHotDogComponent,
  UserComponent,
  CreateUserComponent,
  CustomerComponent,
  CreateCustomerComponent,
  OrderItemComponent,
  CreateOrderItemComponent,
  OrdersComponent,
  CreateOrdersComponent,
  HomePageComponent
],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(router:Router) {
    router.navigate(['/homepage']);
  }
}
