import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HotDogsComponent } from './hot-dogs/hot-dogs.component';
import { CreateHotDogComponent } from './create-hot-dog/create-hot-dog.component';
import { UserComponent } from './user/user.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { CustomerComponent } from './customer/customer.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { OrderItemComponent } from './order-item/order-item.component';
import { CreateOrderItemComponent } from './create-order-item/create-order-item.component';
import { OrdersComponent } from './orders/orders.component';
import { CreateOrdersComponent } from './create-orders/create-orders.component';
import { HomePageComponent } from './home-page/home-page.component';





const routes: Routes = [
  {
    path: '',
    redirectTo: 'hot-dogs',
    pathMatch: 'full'
  },

  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {path:'hot-dogs',component:HotDogsComponent},
  {path:'create-hot-dog',component:CreateHotDogComponent},
  {path:'users',component:UserComponent},
  {path:'create-users',component:CreateUserComponent},
  {path:'customer',component:CustomerComponent},
  {path:'create-customer',component:CreateCustomerComponent},
  {path:'orderitem',component:OrderItemComponent},
  {path:'create-orderitem',component:CreateOrderItemComponent},
  {path:'orders',component:OrdersComponent},
  {path:'create-orders',component:CreateOrdersComponent},
  {path:'homepage',component:HomePageComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
