import { Component, OnInit } from '@angular/core';
import { Orders } from '../shared/orders.model';
import { APIService } from '../shared/api.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit {

  formData:Orders;
  OrderList: Orders[];
    constructor(
      public router:Router,
      public OrderService: APIService) { }
  
    ngOnInit():void{
      
      this.OrderService.getOrderList().then(res => this.OrderList = res as Orders[]);
  
      this.formData={
        OrderID:0,
        OrderNo:'',
        CustomerID:0,
        PayMethod:''
      }
    }
  
    DeleteOrder(id:number){
      this.OrderService.DeleteOrder(id).subscribe(res =>{
        this.resetForm();
        this.refreshList();
      })
    }
  
    resetForm() {
      this.formData = {
        OrderID:0,
        OrderNo:null,
        CustomerID:0,
        PayMethod:null,
      }
    }
  
    refreshList() {
      this.OrderService.getOrderList().then(res => this.OrderList = res as Orders[]);
    }
    GoHome(){
      this.router.navigate(['/homepage'])
    }
    AddOrder(){
      this.router.navigate(['/create-orders']);
    }
    SaveOrder() {
      this.OrderService.UpdateOrder(this.formData).subscribe(res => {
        console.log(res);
        this.resetForm();
        this.refreshList();
      })
    }
    EditOrder(hdObj : Orders){
      // this.router.navigate(['/update-order-item', {id : HotDogID}])
      this.resetForm();
      this.refreshList();
      this.formData.OrderID = hdObj.OrderID;
      this.formData.OrderNo = hdObj.OrderNo;
      this.formData.CustomerID = hdObj.CustomerID;
      this.formData.PayMethod = hdObj.PayMethod;
     
    }
  }