import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { APIService } from '../shared/api.service';
import { HotDog } from '../shared/hot-dog.model';

@Component({
  selector: 'app-create-hot-dog',
  templateUrl: './create-hot-dog.component.html',
  styleUrls: ['./create-hot-dog.component.scss'],
})
export class CreateHotDogComponent implements OnInit {

  constructor(
    private formbuilder: FormBuilder,
    public router : Router,
    public hotdogservice:APIService
  ) { }

  HotDogForm:any;
  data = false;
  message : string;
  ngOnInit(){
  
    this.HotDogForm = this.formbuilder.group({
      HotDogName:['',[Validators.required]],
      HotDogPrice:['',[Validators.required]]
    })
    this.hotdogservice.formData={
      HotDogID:0,
      Name : '',
      Price: 0
    }
  }
  SaveHotDog(){
    
    this.router.navigate(['/hot-dogs'])
    console.log(this.hotdogservice.formData);
    this.hotdogservice.AddHotDog(this.hotdogservice.formData).subscribe(res => {
      console.log(res);

      
    })
  }
  onFormSubmit(HotDogForm){
    const hotdog = HotDogForm.value;
    this.CreateHotDog(hotdog)
  }

  CreateHotDog(hotdog : HotDog){
    this.hotdogservice.AddHotDog(hotdog).subscribe(()=>{
      this.data = true;
      this.message = 'Saved';
      this.HotDogForm.reset();
    })
  }
  GoBackToList(){
    this.router.navigate(['/hot-dogs'])
  }

}
