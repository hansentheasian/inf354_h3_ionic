import { Component, OnInit } from '@angular/core';
import { User } from '../shared/user.model';
import { APIService } from '../shared/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {

  formData:User;
  UserList:User[];
  

  constructor(
  public UserService:APIService,
  private router:Router
  ) { }

  ngOnInit() {
    this.resetForm();
    this.refreshList();
  }
  
  refreshList(){
    this.UserService.getUserList().then(res => this.UserList = res as User[]);
  }
 
  resetForm() {
    this.formData = {
      UserID: 0,
      Name: '',
      Surname: '',
      Email: '',
      Password: ''
    }
  }
  GoHome(){
    this.router.navigate(['/homepage'])
  }
  AddUser(){
    this.router.navigate(['/create-users'])
  }
  DeleteUser(id:number){
    this.UserService.DeleteUser(id).subscribe(res =>{
      this.resetForm();
      this.refreshList();
    })
  }
  EditUser(hdObj : User){
    // this.router.navigate(['/update-order-item', {id : HotDogID}])
    this.resetForm();
    this.refreshList();
    this.formData.UserID = hdObj.UserID;
    this.formData.Name = hdObj.Name;
    this.formData.Surname = hdObj.Surname;
    this.formData.Email = hdObj.Email;
    this.formData.Password = hdObj.Password;
  }

  SaveUser() {
    this.UserService.UpdateUser(this.formData).subscribe(res => {
      console.log(res);
      this.resetForm();
      this.refreshList();
    })
  }
  
  
}
