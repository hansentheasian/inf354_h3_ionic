export class User {

    constructor(
        public UserID :number,
        public Name :string,
        public Surname: string,
        public Email: string,
        public Password : string
     ){ }
}
