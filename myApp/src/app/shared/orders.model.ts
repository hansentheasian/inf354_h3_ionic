export class Orders {

    constructor(
        public OrderID: number,
        public OrderNo :string,
        public CustomerID: number,
        public PayMethod: string
     ){ }
}
