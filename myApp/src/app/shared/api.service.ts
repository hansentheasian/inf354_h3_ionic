import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { HotDog } from './hot-dog.model';
import { User } from './user.model';
import { Customer } from './customer.model';
import { Orderitems } from './orderitems.model';
import { Orders } from './orders.model';



@Injectable({
  providedIn: 'root'
})


export class APIService {
  formData:HotDog;
  formData2:User;
  formData3:Customer;
  formData4:Orderitems;
  formData5:Orders
  constructor(private http:HttpClient) { }

    getHotDogList(){
    return this.http.get(environment.apiURL+'/HotDog/GetHotDogs').toPromise();
    }
    AddHotDog(newHotDog : HotDog){
     return this.http.post(environment.apiURL + '/HotDog/PostHotDog',newHotDog);
    }
    UpdateHotDog(newHotDog:HotDog){
      return this.http.put(environment.apiURL+'/HotDog/' + newHotDog.HotDogID, newHotDog);
    }
    DeleteHotDog(id:number){
      return this.http.delete(environment.apiURL+'/HotDog/'+id);
    }




    getUserList(){
      return this.http.get(environment.apiURL +'/User/GetUsers').toPromise();
    }
    DeleteUser(id:number){
      return this.http.delete(environment.apiURL+'/User/' + id);
    }
    AddUser(newUser:User){
      return this.http.post(environment.apiURL+'/User/PostUser', newUser);
    }
    UpdateUser(newUser:User){
      return this.http.put(environment.apiURL+'/User/' + newUser.UserID, newUser);
    }


    
  getCustomerList() {

    return this.http.get(environment.apiURL + '/Customer/GetCustomers').toPromise();
  }

  DeleteCustomer(id: number) {
    return this.http.delete(environment.apiURL + '/Customer/' + id)
  }


  AddCustomer(newCustomer: Customer) {
    return this.http.post(environment.apiURL + '/Customer/PostCustomer', newCustomer);
  }

  UpdateCustomer(newCustomer: Customer) {
    return this.http.put(environment.apiURL + '/Customer/' + newCustomer.CustomerID, newCustomer);
  }



  AddOrderItem(newOrderItem:Orderitems){
    return this.http.post(environment.apiURL + '/OrderItem/PostOrderItem', newOrderItem);
  }
  DeleteOrderItem(id:number){
    return this.http.delete(environment.apiURL+'/OrderItem/' + id);
  }

  UpdateOrderItem(newOrderItem:Orderitems){
    return this.http.put(environment.apiURL+'/OrderItem/' + newOrderItem.OrderItemID, newOrderItem);
  }
  getOrderItemList(){
    return this.http.get(environment.apiURL+'/OrderItem/GetOrderItems').toPromise();
  }


  getOrderList(){
    return this.http.get(environment.apiURL+'/Order/GetOrders').toPromise();
  }
  AddOrder(newOrder : Orders){
    return this.http.post(environment.apiURL + '/Order/PostOrder',newOrder);
   }
   DeleteOrder(id:number){
    return this.http.delete(environment.apiURL+'/Order/' + id);
  }

  UpdateOrder(newOrder:Orders){
    return this.http.put(environment.apiURL+'/Order/' + newOrder.OrderID, newOrder);
  }
}

