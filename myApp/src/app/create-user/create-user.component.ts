import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { APIService } from '../shared/api.service';
import { User } from '../shared/user.model';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
})
export class CreateUserComponent implements OnInit {

  
  constructor(
    private formbuilder: FormBuilder,
    public router : Router,
    public userservice:APIService
  ) { }

  UserForm:any;
  data = false;
  message : string;
  ngOnInit(){
  
    this.UserForm = this.formbuilder.group({
      Name:['',[Validators.required]],
      Surname:['',[Validators.required]],
      Email:['',[Validators.required]],
      Password:['',[Validators.required]]
    })
    this.userservice.formData2={
      UserID:0,
      Name : '',
      Surname: '',
      Email: '',
      Password: ''
    }
  }
  SaveUser(){
    
    this.router.navigate(['/users'])
    console.log(this.userservice.formData);
    this.userservice.AddUser(this.userservice.formData2).subscribe(res => {
      console.log(res);

      
    })
  }
  onFormSubmit(UserForm){
    const user = UserForm.value;
    this.AddUser(user)
  }

  AddUser(user: User){
    this.userservice.AddUser(user).subscribe(()=>{
      this.data = true;
      this.message = 'Saved';
      this.UserForm.reset();
    })
  }
  GoBackToList(){
    this.router.navigate(['/users'])
  }

}
